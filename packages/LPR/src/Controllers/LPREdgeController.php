<?php
namespace Ironyun\LPR\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Ironyun\LPR\Models\{LPRTarget,LPREvent,LPRCategory};
use Ironyun\LPR\Helper\LPRRetrieveConvert;

class LPREdgeController extends Controller
{   
    public function dashboard(Request $request, $search = null)
    {             
        $matches = array();
        
        /* target
         * $matches['plateNumber'] = '1';
        $matches['category'] = 'Prof. Fiona Gulgowski';
        $matches['sort'] = 'createDate,asc';
        $matches['size'] = $request->input('length');
        $matches['page'] = $request->input('start') / $request->input('length');
        $request->merge(['page' => ($request->input('start') / $request->input('length')) + 1]);*/
        
        //Event
        $matches['start'] = '1970-01-01 00:00:00';
        $matches['end'] = '2021-05-06 00:00:00';
        $matches['footageIds'] = '2';
        //$matches['categories'] = 'Patience Auer,Cassandra Haley';
        //$matches['cameraIds'] = '5';
        //$matches['characters'] = '25';
        //$matches['make'] = 'Praga Baby';
        //$matches['model'] = 'FR-V';
        //$matches['type'] = 'bus';
        $matches['sort'] = 'datetime,desc';
        $matches['size'] = $request->input('length');
        $length = $request->input('length') == 0 ? 10 : $request->input('length');
        $request->merge(['page' => ($request->input('start') / $length) + 1]);
        
        
        $retrieve = new LPRRetrieveConvert();
        $data = $retrieve->getData(['apiPath' => 'lpr/plates', 'matches' => $matches]);
        
        foreach ($data as $v){
            isset($v->target) ? $v->target->category : '';
        }
        return view('lpr::dashboard', array('data' => $data));
    }
    
    public function test(Request $request, $search = null)
    {             
        $data = LPREvent::all();
        foreach ($data as $v){
            isset($v->target) ? $v->target->category : '';
        }
        return view('lpr::dashboard', array('data' => $data));
    }
}

