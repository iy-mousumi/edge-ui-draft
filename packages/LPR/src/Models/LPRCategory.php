<?php
namespace Ironyun\LPR\Models;

use Illuminate\Database\Eloquent\Model;

class LPRCategory extends Model
{   
    protected $table = 'lpr_category';
    public $timestamps = false;
    
    
}