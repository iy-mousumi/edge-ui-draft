<?php
namespace Ironyun\LPR\Helper;

use App\Helper\retrieveConvert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use Log;
use Ironyun\LPR\Models\{LPRCategory,LPRTarget,LPREvent};

class LPRRetrieveConvert extends retrieveConvert{
    
    public $mapping = [
        'lpr/categories' => ['ORM' => LPRCategory::class, 'function' => 'getCategories'],
        'lpr/targets' => ['ORM' => LPRTarget::class, 'function' => 'getTargets'],
        'lpr/plates' => ['ORM' => LPREvent::class, 'function' => 'getHistroy'],
    ];
    
    public function useDB($param)
    {
        /*$param[
         apiPath : lpr/targets,
         json : false,
         matches : ['size' => 50, 'page' => 0 ...] //query conditions
         ]
         */
        return call_user_func('\Ironyun\LPR\Helper\LPRRetrieveConvert::' . $this->mapping[$param['apiPath']]['function'], $param);
    }
    
    private function getCategories($param){
        $ORM = $this->mapping[$param['apiPath']]['ORM'];
        return $ORM::all();
    }
    
    private function getTargets($param){
        $ORM = $this->mapping[$param['apiPath']]['ORM'];
        
        $ret = $ORM::query();//Builder
        if (isset($param['matches']['plateNumber']))
            $ret = $ret->where('plateNumber', 'like', '%' . $param['matches']['plateNumber'] . '%');
            
        if (isset($param['matches']['category'])) {
            $name = $param['matches']['category'];
            $ret = $ret->whereHas('category', function ($query) use ($name) {
                $query->where('name', $name);
            });
        }
        
        $order = explode(',', $param['matches']['sort']);
        $ret = $ret->orderBy($order[0],$order[1])->paginate($param['matches']['size'])->getCollection();//Builder to Collection
        return $ret;
    }
    
    private function getHistroy($param){
        $ORM = $this->mapping[$param['apiPath']]['ORM'];
        
        $ret = $ORM::whereBetween('datetime', [date($param['matches']['start']), date($param['matches']['end'])]);
        
        if (isset($param['matches']['footageIds'])){
            $footageIds = explode(',', $param['matches']['footageIds']);
            $ret->where(function ($query) use($footageIds) {
                foreach($footageIds as $id) {
                    $query->orWhere('footageId', '=', $id);
                }
                //dd($query->get());
            });
        }
        
        if (isset($param['matches']['cameraIds'])){
            $cameraIds = explode(',', $param['matches']['cameraIds']);
            $ret->where(function ($query) use($cameraIds) {
                foreach($cameraIds as $id) {
                    $query->orWhere('cameraId', '=', $id);
                }
            });
        }
        
        if (isset($param['matches']['categories'])) {
            $categories = explode(',', $param['matches']['categories']);
            $ret->whereHas('target.category', function ($query) use ($categories){
                $query->where(function ($q) use($categories) {
                    foreach($categories as $name) {
                        $q->orWhere('name', '=', $name);
                    }
                });
            });
        }
        
        if (isset($param['matches']['characters']))
            $ret->where('characters', 'like', '%' . $param['matches']['characters'] . '%');
        
        if (isset($param['matches']['make']))
            $ret->where('make', '=', $param['matches']['make']);
        
        if (isset($param['matches']['model']))
            $ret->where('model', '=', $param['matches']['model']);
            
        if (isset($param['matches']['type']))
            $ret->where('type', '=', $param['matches']['type']);
        
        //Color query miss
        $order = explode(',', $param['matches']['sort']);
        $ret = $ret->orderBy($order[0],$order[1])->paginate($param['matches']['size'])->getCollection();//Builder to Collection
        return $ret;
    }
}