<?php

return [
    "register.title.welcome" => "歡迎使用本產品，請完成線上驗證並取得授權金鑰。",
    "register.title.check_email" => "確認產品序號及電子郵箱",
    "register.title.verify_email" => "驗證電子郵箱",
    "register.title.activationCode" => "16 位授權金鑰 (License Key)",
    "register.title.setup" => "下載軟體及安裝註冊﹕",    
    
    "register.label.sn" => "產品序號",
    "register.label.email" => "電子郵箱",
    "register.label.captcha" => "驗證碼",
    
    "register.placeholder.captcha" => "請輸入驗證碼",
    "register.placeholder.vcode" => "電子郵箱驗證碼",

    "register.button.sendEmail" => "發送驗證信",
    "register.button.confirm" => "確認",    

    "register.label.receive_email" => "請查看您的電子郵箱",
    "register.label.enterCapchaIn5Min" => "請於5分鐘內輸入5位數字的驗證碼",
    "register.label.verificationSuccess" => "驗證成功。",
    "register.label.captchaRemind1" => "如果您尚未收到驗證信函，請查閱是否被歸類為垃圾信件，或者重新確認您的Email是否輸入正確。",
    "register.label.captchaRemind2" => "驗證成功後，您將收到一封「授權金鑰通知信」。",
    "register.label.toDownload" => "請點擊此下載連結",
    "register.label.downloadLink" => "選擇您需要的軟體版本並下載安裝，並以一組Email及自訂密碼註冊登入。",
    "register.label.remind" => "輸入16位授權金鑰以啟用 BullGuard 產品。",
    
    "global.error.label.wrongEmailFormat" => "請輸入正確的郵件格式",
    "global.error.label.pleaseEnterSN" => "請輸入正確的產品序號格式",
];