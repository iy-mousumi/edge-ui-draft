<?php

return [
    'required' => ':attribute字段是必填字段。',
    'email' => '必須是一個有效的E-mail地址',
    'captcha' => '驗證碼錯誤!',
    'digits' => '驗證碼必須為:digits位數'
];