@extends('layout.main')

@section('content')
<div id="content" class="container-fulid">
	<table class="table table-striped" id="list">
      <thead>
        <tr>
          <th scope="col">TAB1 - this is tested</th>
          <th scope="col">TAB2</th>
          <th scope="col">TAB3</th>
          <th scope="col">TAB4</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
	
</div>
@endsection

@section('javascript')
 <script type="text/javascript">
 $.ajaxSetup({
     headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
     error: function(jqXHR, textStatus, errorThrown) {
         toastr.error('@lang("lang.app.error.label.msg.lostConnection")');
     }
 });
 
$(document).ready(function () {
	let data = {!! json_encode($data) !!};
	console.log(data);

});
</script>
@endsection