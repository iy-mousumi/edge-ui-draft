<?php
namespace Ironyun\Camera;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\{Route, View};

class CameraServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/Resources/lang', 'camera');
        $this->loadViewsfrom(__DIR__.'/Resources/views', 'camera');
        Route::namespace('Ironyun\Camera\Controllers')->group(__DIR__ . "/Routes/Route.php");
    }
}