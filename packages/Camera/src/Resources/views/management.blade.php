@extends('basic::layout.main')

@section('content')
<div id="content" class="container-fulid">
 	<div class="d-flex">
         	<table class="table table-striped" id="dashboard">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
        
              </tbody>
            </table>
        </div>
        <div class="liveView">
          One of three columns
        </div>
    </div>
</div>
@endsection

@section('javascript')
 <script type="text/javascript">
 $.ajaxSetup({
     headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
     error: function(jqXHR, textStatus, errorThrown) {
         toastr.error('@lang("lang.app.error.label.msg.lostConnection")');
     }
 });
 
$(document).ready(function () {

});
</script>
@endsection