<?php
namespace Ironyun\Camera\Controllers;

use Ironyun\Basic\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class CameraController extends Controller
{   
    public function settings(Request $request)
    {           
        return view('settings::settings', array('options' => $this->options));
    }
}

