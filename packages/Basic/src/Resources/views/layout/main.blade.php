<html>
    <head>
        <title>Main</title>
    </head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/bootstrap/bootstrap.min.css') }}?{{ time() }}"> 
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/style.css') }}?{{ time() }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/images.css') }}?{{ time() }}"> 
		<style>
			.frame{
				width: 100%; 
				height: 315px;
			}
		</style>
    <body>
		@include('basic::layout.navbar')
        <div class="wrapper" style="overflow: hidden;">
            @yield('content')
        </div>
    </body>
    <script src="{{ URL::asset('/assets/bootstrap/bootstrap.min.js') }}?{{ time() }}"></script>
    <script src="{{ URL::asset('/assets/jquery/jquery.min.js') }}?{{ time() }}"></script>
    @stack('scripts')
    @yield('javascript')
</html>