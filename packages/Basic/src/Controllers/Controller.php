<?php

namespace Ironyun\Basic\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Session;
use App\CommonLib;
use Exception;
use Log;
use App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $options;
    public function __construct()
    {
        $this->middleware(function ($request, $next)
        {
            $this->options = $request->get('options');
            return $next($request);
        });
    }
}