<?php
namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * https://github.com/fzaninotto/Faker
     * @return void
     */
    public function run()
    {
        $this->call(LPRSeeder::class);
        
        $this->command->info('LPR table seeded!');
    }
}
