<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LPRSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lpr_category')->truncate();
        $faker = \Faker\Factory::create();
        foreach (range(1,3) as $index) {
            DB::table('lpr_category')->insert([
                'createDate' => $faker->dateTime($max = 'now'),
                'updateDate' => $faker->dateTime($max = 'now'),
                'licensePlateCategoryId' => $faker->uuid,
                'name' => $faker->name,
                'ainvrId' => $faker->randomDigit(),
            ]);
        }
        
        //get list CategoryId array
        $categoryIds = array();
        foreach (DB::table('lpr_category')->get() as $category)
            $categoryIds[] = $category->licensePlateCategoryId;
            
        DB::table('lpr_target')->truncate();
        $faker = \Faker\Factory::create();
        foreach (range(1,20) as $index) {
            DB::table('lpr_target')->insert([
                'createDate' => $faker->dateTime($max = 'now'),
                'updateDate' => $faker->dateTime($max = 'now'),
                'licensePlateTargetId' => $faker->uuid,
                'licensePlateCategoryId' => $faker->randomElement($categoryIds),
                'plateNumber' => $faker->numberBetween($min = 100000, $max = 999999),
                'description' => $faker->realText($maxNbChars = 10, $indexSize = 2),
                'vehicleOwner' => $faker->realText($maxNbChars = 10, $indexSize = 2),
                'address' => $faker->realText($maxNbChars = 10, $indexSize = 2),
                'registrationDate' => $faker->dateTime($max = 'now'),
                'ainvrId' => $faker->randomDigit(),
            ]);
        }
        
        //get target Id array
        $targetIds = array();
        foreach (DB::table('lpr_target')->get() as $target)
            $targetIds[] = $target->licensePlateTargetId;
                
        DB::table('lpr_event')->truncate();
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Faker\Provider\Fakecar($faker));
        foreach (range(1,50) as $index) {
            DB::table('lpr_event')->insert([
                'licensePlateId' => $faker->unique()->numberBetween($min = 1, $max = 100),
                'cameraId' => $faker->randomDigit(),
                'footageId' => $faker->randomDigit(),
                'licensePlateTargetId' => $faker->randomElement($targetIds),
                'sceneId' => $faker->numberBetween($min = 1000, $max = 999999),
                'datetime' => $faker->dateTime($max = 'now'),
                'type' => $faker->randomElement(['car', 'bus', 'motorcycle', 'biclcle', 'truck']),
                'characters' => $faker->numberBetween($min = 100000, $max = 999999),
                'confidence' => $faker->randomFloat($nbMaxDecimals = 1, $min = 0, $max = 1),
                'x' => $faker->numberBetween($min = 0, $max = 1920),
                'y' => $faker->numberBetween($min = 0, $max = 1280),
                'w' => $faker->numberBetween($min = 40, $max = 400),
                'h' => $faker->numberBetween($min = 40, $max = 400),
                'file' => null,//$faker->imageUrl(200, 80, 'cats', true, 'Faker'),
                'property' => $faker->colorName(),
                'make' => ($faker->boolean ? $faker->vehicleBrand : null),
                'model' => ($faker->boolean ? $faker->vehicleModel : null),
                'vehicleType' => $faker->randomElement(['na', 'private']),
                'modified' => $faker->boolean,
                'ainvrId' => $faker->randomDigit(),
                
                //'metadata' => json_encode(["alertIamge" => $faker->imageUrl($width = 640, $height = 480) ] )
            ]);
        }
    }
}
