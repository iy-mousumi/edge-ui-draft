<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLPREventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lpr_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('createDate');
            $table->dateTime('updateDate');
            $table->string('licensePlateCategoryId', 50)->nullable();
            $table->string('name', 100)->nullable();
            $table->integer('ainvrId');
        });
        
        Schema::create('lpr_target', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('createDate');
            $table->dateTime('updateDate');
            $table->string('licensePlateTargetId', 50)->nullable();
            $table->string('licensePlateCategoryId', 50)->nullable();
            $table->string('plateNumber', 10)->nullable();
            $table->string('description', 2000)->nullable();
            $table->string('vehicleOwner', 200)->nullable();
            $table->string('address', 200)->nullable();
            $table->dateTime('registrationDate');
            $table->integer('ainvrId');
        });
        
        Schema::create('lpr_event', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('licensePlateId');
            $table->integer('cameraId');
            $table->integer('footageId');
            $table->string('licensePlateTargetId', 50)->nullable();
            $table->integer('sceneId');
            $table->dateTime('datetime');
            
            $table->string('type', 20)->nullable();
            $table->string('characters', 10)->nullable();
            $table->decimal('confidence', 3, 1)->nullable();
            $table->integer('x');
            $table->integer('y');
            $table->integer('w');
            $table->integer('h');
            $table->string('file', 200)->nullable();
            $table->string('property', 50)->nullable();
            $table->string('make', 100)->nullable();
            $table->string('model', 100)->nullable();
            $table->string('vehicleType', 10)->nullable();
            $table->boolean('modified');
            $table->integer('ainvrId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lpr_category');
        Schema::dropIfExists('lpr_target');
        Schema::dropIfExists('lpr_event');
    }
}
