<?php
namespace App\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Ixudra\Curl\Facades\Curl;
use Exception;
use Log;
use Illuminate\Support\Facades\Config;

class retrieveConvert {
    
    private $apiUrl = 'http://172.16.12.158';
    private $token = 'eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiJhZG1pbiIsImF1ZCI6IiIsImlzcyI6IjkxNGFkMTQ0YzE5ZDhjMzhjYThkNDU5Mzk1MmU3Mjc1ZmUwMjI5MGY5NGIyZmJhMDkxNWQ0MzE5MGU3YzA0MzkiLCJleHAiOjE2MjAyODY5MDYsImlhdCI6MTYyMDIwMDUwNn0.phSrlSq5Yt41-XdNBr38L1X3KAhsnpTcrrGt14P7WK5WiACqnIlo5pet8ZwzUWxBxN0AujNGq6kxCmnADbzoGg';
    
    public function getData($param){
        if (Config::get ('edge.dataSource') == 'mysql')
            return $this->useDB($param);
        else
            return $this->useAPI($param);
    }

    private function useAPI($param)
    {
        /*$param[
            apiPath : lpr/targets,
            json : false,
            matches : ['size' => 50, 'page' => 0 ...] //query conditions
        ]
        */
        if (isset($param['json']) && $param['json'])
            return $this->callApiWithJson($param);
        else
            return $this->callApiWithData($param);
    }
    
    private function callApiWithData($param)
    {
        $response = Curl::to ( $this->apiUrl.'/ainvr/api/' . $param['apiPath'] )
        ->withHeader ( 'x-auth-token:' . $this->token)
        ->withOption ( 'SSL_VERIFYHOST', false )
        ->withData($param['matches'])
        ->returnResponseObject ()
        ->get();
        
        if ($response->status != 200) {
            throw new Exception ( 'Access LPR Target failed' . '(' . $response->status . ')' . $response->content );
        }
        
        $content = json_decode($response->content);
        return isset($content->content) ? $content->content : $content; 
    }
    
    private function callApiWithJson($param)
    {
        $response = Curl::to ( $this->apiUrl.'/ainvr/api/' . $param['url'])
        ->withHeader ( 'x-auth-token:' . $this->token)
        ->withContentType ( 'application/json' )
        ->withData(json_encode($param['matches']))
        ->withOption ( 'SSL_VERIFYHOST', false )
        ->returnResponseObject ()
        ->get();

    }
}