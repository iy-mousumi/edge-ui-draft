<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="#">
		<img src="/images/logo_white.svg" width="149" height="40" alt="">
	</a>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto" id="navbarUl">

        </ul>
    </div>
</nav>

@push('scripts')
<script>

	let basic = {
    	'camera' : {
    		name: "@lang('camera::lang.app.camera.label.camera')",
    		href:  "{{url('/camera/management')}}",
    	}
	};
	let packages = {
		'lpr' : {
			name: "@lang('lpr::lang.app.lpr.label.licensePlateRecognition')",
			href:  "{{url('/lpr/dashboard')}}",
		}
	};
	let options = {!! json_encode($options) !!};
	
	//gen navbar
	for (idx in options.provider){
		if (options.provider[idx] && packages[idx])
			$('#navbarUl').append('<li class="nav-item"><a class="nav-link active" href="' + packages[idx].href + '">' + packages[idx].name + '</a></li>');
	}

    for (idx in options.provider){
    	if (options.provider[idx]  && basic[idx])
    		$('#navbarUl').append('<li class="nav-item"><a class="nav-link active" href="' + basic[idx].href + '">' + basic[idx].name + '</a></li>');
    }
</script>
@endpush