<?php

namespace Ironyun\Basic\Middleware;

use Closure;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Session;
use Log;
use App;
use Exception;

class BasicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    protected $options;
    public function handle($request, Closure $next)
    {
        try{
            $this->initOptions();
            
            if (class_exists(\Ironyun\Camera\CameraServiceProvider::class)) {
                $this->options['provider']['camera'] = true;
            }
            
            if (class_exists(\Ironyun\LPR\LPRServiceProvider::class)) {
                $this->options['provider']['lpr'] = true;
            }
            
            if (class_exists(\Ironyun\Fr\FrServiceProvider::class)) {
                $this->options['provider']['fr'] = true;
            }
            if (class_exists(\Ironyun\Settings\SettingsServiceProvider::class)) {
                $this->options['provider']['settings'] = true;
            }
            
            $request->attributes->add(array('options' => $this->options));
            return $next($request);
        }catch (Exception $e) {
            Log::error($e);
        }
        return false;
    }
    
    private function initOptions(){
        $options['provider'] = array();
        $options['provider']['lpr'] = false;
        $options['provider']['fr'] = false;
        $options['provider']['fall'] = false;
        $options['provider']['settings'] = false;
    }
}