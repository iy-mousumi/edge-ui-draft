<?php
namespace Ironyun\Basic;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\{Route, View};

class BasicServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/Resources/lang', 'basic');
        $this->loadViewsfrom(__DIR__.'/Resources/views', 'basic'); 
        $this->publishes([
            __DIR__.'/Resources/views/css' => public_path('css'),
            __DIR__.'/Resources/views/images' => public_path('images'),
        ], 'public');
        $this->app['router']->pushMiddlewareToGroup('web', \Ironyun\Basic\Middleware\BasicMiddleware::class);
    }
}