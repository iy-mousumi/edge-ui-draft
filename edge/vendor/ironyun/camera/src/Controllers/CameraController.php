<?php
namespace Ironyun\Camera\Controllers;

use Ironyun\Basic\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class CameraController extends Controller
{   
    public function management(Request $request)
    {           
        return view('camera::management', array('options' => $this->options));
    }
}

