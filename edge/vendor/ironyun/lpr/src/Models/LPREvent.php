<?php
namespace Ironyun\LPR\Models;

use Illuminate\Database\Eloquent\Model;

class LPREvent extends Model
{   
    protected $table = 'lpr_event';
    public $timestamps = false;
    
    public function target()
    {
        return $this->belongsTo(LPRTarget::class, 'licensePlateTargetId', 'licensePlateTargetId');
    }
}
