<?php
namespace Ironyun\LPR\Models;

use Illuminate\Database\Eloquent\Model;

class LPRTarget extends Model
{   
    protected $table = 'lpr_target';
    public $timestamps = false;
    
    public function category()
    {
        return $this->belongsTo(LPRCategory::class, 'licensePlateCategoryId', 'licensePlateCategoryId');
    }
}