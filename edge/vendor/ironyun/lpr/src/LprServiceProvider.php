<?php
namespace Ironyun\LPR;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\{Route, View};

class LPRServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->loadTranslationsFrom(__DIR__.'/Resources/lang', 'lpr');
        $this->loadViewsfrom(__DIR__.'/Resources/views', 'lpr'); 
        Route::namespace('Ironyun\LPR\Controllers')->group(__DIR__ . "/Routes/Route.php");
    }
}
